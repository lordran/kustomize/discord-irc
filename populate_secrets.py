#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
import subprocess
import re
import json

working_directory = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(working_directory, 'config.json.template')) as json_template:
    config = json.load(json_template)

ret = subprocess.run(['pass', 'perso/Servers/Discord/LE_IRC_bot_token'], capture_output=True, encoding='utf8')
config[0]['discordToken'] = ret.stdout.strip()

r = re.compile(r'^([├─└]+) (.+)$')
ls_ret = subprocess.run(['pass', 'ls', 'perso/Servers/Discord/webhooks/lost-edens/'], capture_output=True, encoding='utf8')
for line in ls_ret.stdout.splitlines():
    match = r.match(line)
    if match:
        webhook_name = match.groups()[1]
        pass_sub = os.path.join('perso/Servers/Discord/webhooks/lost-edens/', webhook_name)
        ret = subprocess.run(['pass', pass_sub], capture_output=True, encoding='utf8')
        config[0]['webhooks']['#' + webhook_name] = ret.stdout.strip()
        if webhook_name == 'general':
            config[0]['channelMapping']['#general'] = '#lost-edens'
        else:
            config[0]['channelMapping']['#' + webhook_name] = '#lost-edens-' + webhook_name

with open(os.path.join(working_directory, 'config.json'), 'w') as json_config:
    json.dump(config, json_config, indent=2)
